package info.earty.gateway.infrastructure.jaxrs.cxf;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

//NOTE EA 9/14/2021 - This exception mapper should handle any AccessDeniedException that doesn't
//  get picked up by spring's AccessDeniedEntryPoint. For example if the 'hasRole' was configured
//  in a @PreAuthorize annotation as opposed to in the WebSecurityConfig.
@Provider
public class AccessDeniedExceptionMapper implements ExceptionMapper<AccessDeniedException> {

    public Response toResponse(AccessDeniedException exception) {
        if (SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
            return Response.status(Response.Status.UNAUTHORIZED).header(HttpHeaders.WWW_AUTHENTICATE, "Bearer").build();
        }
        else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

}
