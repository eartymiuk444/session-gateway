package info.earty.gateway.infrastructure.jaxrs.cxf;

import info.earty.image.presentation.ImageCommandApi;
import info.earty.image.presentation.ImageQueryApi;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class ImageClientConfiguration {

    @Value("${info.earty.gateway.image.service.host}")
    private String imageServiceHost;

    @Value("${info.earty.gateway.image.service.port}")
    private String imageServicePort;

    @Bean
    public ImageCommandApi imageCommandApi() {
        return clientApiHelper(ImageCommandApi.class);
    }

    @Bean
    public ImageQueryApi imageQueryApi() {
        return clientApiHelper(ImageQueryApi.class);
    }

    private <T> T clientApiHelper(Class<T> clazz) {
        JAXRSClientFactoryBean clientFactory = new JAXRSClientFactoryBean();
        clientFactory.setAddress(String.format("http://%s:%s/", imageServiceHost, imageServicePort));
        clientFactory.setServiceClass(clazz);
        return (T) clientFactory.create();
    }
}
