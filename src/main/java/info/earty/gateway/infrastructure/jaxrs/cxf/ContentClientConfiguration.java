package info.earty.gateway.infrastructure.jaxrs.cxf;

import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;
import info.earty.sitemenu.presentation.*;
import info.earty.workingpage.presentation.*;
import info.earty.workingcard.presentation.*;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class ContentClientConfiguration {

    @Value("${info.earty.gateway.site-menu.service.host}")
    private String siteMenuHost;

    @Value("${info.earty.gateway.site-menu.service.port}")
    private String siteMenuPort;
    
    @Value("${info.earty.gateway.working-page.service.host}")
    private String workingPageHost;

    @Value("${info.earty.gateway.working-page.service.port}")
    private String workingPagePort;

    @Value("${info.earty.gateway.working-card.service.host}")
    private String workingCardHost;

    @Value("${info.earty.gateway.working-card.service.port}")
    private String workingCardPort;

    private final JacksonJsonProvider jacksonJsonProvider;

    @Bean
    public SiteMenuCommandApi siteMenuCommandApi() {
        return clientApiHelper(SiteMenuCommandApi.class, siteMenuHost, siteMenuPort);
    }

    @Bean
    public SiteMenuQueryApi siteMenuQueryApi() {
        return clientApiHelper(SiteMenuQueryApi.class, siteMenuHost, siteMenuPort);
    }

    @Bean
    public WorkingPageCommandApi workingPageCommandApi() {
        return clientApiHelper(WorkingPageCommandApi.class, workingPageHost, workingPagePort);
    }

    @Bean
    public WorkingPageQueryApi workingPageQueryApi() {
        return clientApiHelper(WorkingPageQueryApi.class, workingPageHost, workingPagePort);
    }

    @Bean
    public WorkingCardCommandApi workingCardCommandApi() {
        return clientApiHelper(WorkingCardCommandApi.class, workingCardHost, workingCardPort);
    }

    @Bean
    public WorkingCardQueryApi workingCardQueryApi() {
        return clientApiHelper(WorkingCardQueryApi.class, workingCardHost, workingCardPort);
    }

    private <T> T clientApiHelper(Class<T> clazz, String host, String port) {
        JAXRSClientFactoryBean clientFactory = new JAXRSClientFactoryBean();
        clientFactory.setAddress(String.format("http://%s:%s/", host, port));
        clientFactory.setProvider(jacksonJsonProvider);
        clientFactory.setServiceClass(clazz);
        return (T) clientFactory.create();
    }

}
