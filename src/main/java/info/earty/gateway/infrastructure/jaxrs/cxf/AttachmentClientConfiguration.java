package info.earty.gateway.infrastructure.jaxrs.cxf;

import info.earty.attachment.presentation.AttachmentCommandApi;
import info.earty.attachment.presentation.AttachmentQueryApi;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class AttachmentClientConfiguration {

    @Value("${info.earty.gateway.attachment.service.host}")
    private String attachmentServiceHost;

    @Value("${info.earty.gateway.attachment.service.port}")
    private String attachmentServicePort;

    @Bean
    public AttachmentCommandApi attachmentCommandApi() {
        return clientApiHelper(AttachmentCommandApi.class);
    }

    @Bean
    public AttachmentQueryApi attachmentQueryApi() {
        return clientApiHelper(AttachmentQueryApi.class);
    }

    private <T> T clientApiHelper(Class<T> clazz) {
        JAXRSClientFactoryBean clientFactory = new JAXRSClientFactoryBean();
        clientFactory.setAddress(String.format("http://%s:%s/", attachmentServiceHost, attachmentServicePort));
        clientFactory.setServiceClass(clazz);
        return (T) clientFactory.create();
    }
}
