package info.earty.gateway.infrastructure.spring.security.oidc;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ContinueAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final ContinueRepository continueRepository;
    private final RedirectStrategy redirectStrategy;

    public ContinueAuthenticationSuccessHandler(ContinueRepository continueRepository) {
        super();
        this.continueRepository = continueRepository;
        this.redirectStrategy = new DefaultRedirectStrategy();
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        String continueUri = this.continueRepository.removeContinueUri(request, response);

        if (continueUri != null) {
            this.redirectStrategy.sendRedirect(request, response, continueUri);
        }
    }

    protected RedirectStrategy getRedirectStrategy() {
        return this.redirectStrategy;
    }

    protected ContinueRepository getContinueRepository() {
        return this.continueRepository;
    }
}
