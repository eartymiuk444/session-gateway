package info.earty.gateway.infrastructure.spring.security.oidc;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.ClientRegistrations;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.csrf.*;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableMethodSecurity
public class WebSecurityConfig {

    //Management - https://console.developers.google.com - eartymiuk@gmail.com - credentials (Client ID for Web application)
    private final static String GOOGLE_REGISTRATION_ID = "google";
    private final static String GOOGLE_CLIENT_NAME = "Google";
    private final static String GOOGLE_ISSUER_LOCATION = "https://accounts.google.com";
    private final static String GOOGLE_CLIENT_ID = "156463960783-v0t8lg03mc2lpmqjoqu9p6227o4op74a.apps.googleusercontent.com";
    @Value("${info.earty.gateway.key.secret.google}")
    private String googleSecret;

    //Management - https://portal.azure.com/#home - eartymiuk@gmail.com - Azure services - App Registrations
    private final static String MICROSOFT_REGISTRATION_ID = "microsoft";
    private final static String MICROSOFT_CLIENT_NAME = "Microsoft";
    private final static String MICROSOFT_TENANT_ID = "c409dec6-8463-4627-a7bc-72d8e32e0216";
    private final static String MICROSOFT_ISSUER_LOCATION = "https://login.microsoftonline.com/" + MICROSOFT_TENANT_ID + "/v2.0";
    private final static String MICROSOFT_CLIENT_ID = "dabe7a64-ff3e-472a-ba69-2fb0c8d313f7";
    @Value("${info.earty.gateway.key.secret.microsoft}")
    private String microsoftSecret;

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.addFilterBefore(new OAuth2LoginContinuePersistenceFilter(this.authorizationRequestResolver(),
                        this.continueRepository()), OAuth2AuthorizationRequestRedirectFilter.class)
                .securityMatcher(new AntPathRequestMatcher("/**"))
                .authorizeHttpRequests(authorize -> authorize
                        //authentication
                        //NOTE EA 9/14/2021 - logout is configured below and the antmatcher config here doesn't appear to have any effect.
                        //.antMatchers("/api/auth/logout").authenticated()
                        .requestMatchers(new AntPathRequestMatcher("/api/auth/login/oauth2/**")).permitAll()
                        //api documentation
                        .requestMatchers(
                                new AntPathRequestMatcher("/api/api-docs"),
                                new AntPathRequestMatcher("/api/openapi.json"),
                                new AntPathRequestMatcher("/api/swagger-ui.css"),
                                new AntPathRequestMatcher("/api/index.css"),
                                new AntPathRequestMatcher("/api/swagger-ui-bundle.js"),
                                new AntPathRequestMatcher("/api/swagger-ui-standalone-preset.js"),
                                new AntPathRequestMatcher("/api/swagger-initializer.js")).hasRole("ADMIN")
                        //api
                        .requestMatchers(new AntPathRequestMatcher("/api/user/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/api/site-menu/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/api/working-page/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/api/working-card/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/api/image/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/api/attachment/**")).permitAll()
                        //everything else
                        .anyRequest().denyAll())
                .oauth2Login(oauth2 -> oauth2
                        .authorizationEndpoint(authorization -> authorization
                                .authorizationRequestResolver(this.authorizationRequestResolver()))
                        .redirectionEndpoint(redirection -> redirection
                                .baseUri("/api/auth/login/oauth2/code/*"))
                        .userInfoEndpoint(userInfo -> userInfo
                                .oidcUserService(oidcUserService()))
                        .successHandler(this.authenticationSuccessHandler()))
                .requestCache(requestCache ->
                        requestCache.requestCache(this.sessionRequestCache()))
                //NOTE EA 9/12/2021 - We only want to redirect to login for /api/api-docs
                .exceptionHandling(exceptionHandling ->
                        exceptionHandling.defaultAuthenticationEntryPointFor(
                                new LoginUrlAuthenticationEntryPoint("/api/auth/login/oauth2/authorization/google"),
                                new AntPathRequestMatcher("/api/api-docs")))
                //NOTE EA 9/14/2021 - This exception handler should handle any AccessDeniedException that doesn't
                //  get picked up by the cxf AccessDeniedExceptionMapper. For example if the 'hasRole' was configured
                //  in the WebSecurityConfig as opposed to in a @PreAuthorize annotation.
                .exceptionHandling(exceptionHandling ->
                        exceptionHandling.defaultAuthenticationEntryPointFor(
                                new AccessDeniedEntryPoint(), new AntPathRequestMatcher("/**")))
                .csrf(csrf -> csrf
                        .csrfTokenRepository(this.csrfTokenRepository())
                        .csrfTokenRequestHandler(this.csrfTokenRequestAttributeHandler()))
                .logout(logout -> logout
                        .logoutUrl("/api/auth/logout")
                        .logoutSuccessHandler((request, response, authentication) -> response.setStatus(HttpServletResponse.SC_OK)));

        return http.build();
    }

    @Bean
    public OAuth2AuthorizationRequestResolver authorizationRequestResolver() {
        return new DefaultOAuth2AuthorizationRequestResolver(this.clientRegistrationRepository(),
                "/api/auth/login/oauth2/authorization");
    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository(googleClientRegistration(this.googleSecret),
                microsoftClientRegistration(this.microsoftSecret));
    }

    @Bean
    public HttpSessionContinueRepository continueRepository() {
        return new HttpSessionContinueRepository();
    }

    @Bean
    public ContinueOrSavedRequestAuthenticationSuccessHandler authenticationSuccessHandler() {
        return new ContinueOrSavedRequestAuthenticationSuccessHandler(this.continueRepository(), this.sessionRequestCache());
    }

    @Bean
    public HttpSessionRequestCache sessionRequestCache() {
        HttpSessionRequestCache requestCache = new HttpSessionRequestCache();
        requestCache.setMatchingRequestParameterName(null);
        return requestCache;
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
        return CookieCsrfTokenRepository.withHttpOnlyFalse();
    }

    @Bean CsrfTokenRequestAttributeHandler csrfTokenRequestAttributeHandler() {
        CsrfTokenRequestAttributeHandler requestHandler = new CsrfTokenRequestAttributeHandler();
        requestHandler.setCsrfRequestAttributeName(null);
        return requestHandler;
    }

    private static ClientRegistration googleClientRegistration(String secret) {

        ClientRegistration c = ClientRegistrations.fromOidcIssuerLocation(GOOGLE_ISSUER_LOCATION)
                .registrationId(GOOGLE_REGISTRATION_ID)
                .clientName(GOOGLE_CLIENT_NAME)
                .clientId(GOOGLE_CLIENT_ID)
                .clientSecret(secret)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("{baseUrl}/api/auth/login/oauth2/code/{registrationId}")
                .scope("openid")
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .build();

        return c;
    }

    private static ClientRegistration microsoftClientRegistration(String secret) {

        ClientRegistration c = ClientRegistrations.fromOidcIssuerLocation(MICROSOFT_ISSUER_LOCATION)
                .registrationId(MICROSOFT_REGISTRATION_ID)
                .clientName(MICROSOFT_CLIENT_NAME)
                .clientId(MICROSOFT_CLIENT_ID)
                .clientSecret(secret)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("{baseUrl}/api/auth/login/oauth2/code/{registrationId}")
                .scope("openid")
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .build();

        return c;
    }

    private static OAuth2UserService<OidcUserRequest, OidcUser> oidcUserService() {
        final OidcUserService delegate = new OidcUserService();

        return (userRequest) -> {
            // Delegate to the default implementation for loading a user
            OidcUser oidcUser = delegate.loadUser(userRequest);

            OAuth2AccessToken accessToken = userRequest.getAccessToken();
            Set<GrantedAuthority> mappedAuthorities = new HashSet<>();

            if (oidcUser.getIssuer().toString().equals(GOOGLE_ISSUER_LOCATION) &&
                    oidcUser.getIdToken().getSubject().equals("114846527089482686318")) {
                GrantedAuthority adminAuth = new SimpleGrantedAuthority("ROLE_ADMIN");
                mappedAuthorities.add(adminAuth);
            }
            else if (oidcUser.getIssuer().toString().equals(MICROSOFT_ISSUER_LOCATION) &&
                    oidcUser.getIdToken().getSubject().equals("8QudA63FUhw_5sJB5fRSuRH9_0m7DwzVM6oNDvzMtPU")) {
                GrantedAuthority adminAuth = new SimpleGrantedAuthority("ROLE_ADMIN");
                mappedAuthorities.add(adminAuth);
            }

            if (oidcUser.getIdToken().getIssuer().toString().equals(GOOGLE_ISSUER_LOCATION)) {
                GrantedAuthority googleAuth = new SimpleGrantedAuthority("ROLE_GOOGLE_USER");
                GrantedAuthority userAuth = new SimpleGrantedAuthority("ROLE_USER");
                mappedAuthorities.add(googleAuth);
                mappedAuthorities.add(userAuth);
            }
            else if (oidcUser.getIssuer().toString().equals(MICROSOFT_ISSUER_LOCATION)) {
                GrantedAuthority adminAuth = new SimpleGrantedAuthority("ROLE_MICROSOFT_USER");
                GrantedAuthority userAuth = new SimpleGrantedAuthority("ROLE_USER");
                mappedAuthorities.add(adminAuth);
                mappedAuthorities.add(userAuth);
            }
            else {
                GrantedAuthority userAuth = new SimpleGrantedAuthority("ROLE_USER");
                mappedAuthorities.add(userAuth);
            }

            oidcUser = new DefaultOidcUser(mappedAuthorities, oidcUser.getIdToken(), oidcUser.getUserInfo());

            return oidcUser;
        };
    }

}
