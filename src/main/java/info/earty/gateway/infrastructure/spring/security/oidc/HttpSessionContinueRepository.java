package info.earty.gateway.infrastructure.spring.security.oidc;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class HttpSessionContinueRepository implements ContinueRepository {

    private static final String CONTINUE_PARAM_NAME = "continue";
    private static final String CONTINUE_ATTRIBUTE_NAME = "CONTINUE";

    public void saveContinueUri(HttpServletRequest request, HttpServletResponse response) {
        String continueParam = request.getParameter(CONTINUE_PARAM_NAME);
        if (continueParam != null) {
            UriComponents uriComponents = UriComponentsBuilder.fromUriString(continueParam).build();

            if (uriComponents.getHost() == null &&
                    uriComponents.getScheme() == null &&
                    uriComponents.getPort() == -1 &&
                    uriComponents.getQuery() == null &&
                    uriComponents.getUserInfo() == null &&
                    uriComponents.getPath() != null) {
                String continueUriString = UriComponentsBuilder.fromPath(uriComponents.getPath()).fragment(uriComponents.getFragment()).build().toString();
                request.getSession().setAttribute(CONTINUE_ATTRIBUTE_NAME, continueUriString);
            }
        }
    }

    public String loadContinueUri(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (session != null) ? (String) session.getAttribute(CONTINUE_ATTRIBUTE_NAME) : null;
    }

    public String removeContinueUri(HttpServletRequest request, HttpServletResponse response) {
        String continueUri = loadContinueUri(request);
        HttpSession session = request.getSession();
        if (session != null) {
            session.removeAttribute(CONTINUE_ATTRIBUTE_NAME);
        }
        return continueUri;
    }

}
