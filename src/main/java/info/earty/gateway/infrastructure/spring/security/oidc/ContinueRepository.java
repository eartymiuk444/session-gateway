package info.earty.gateway.infrastructure.spring.security.oidc;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface ContinueRepository {
    void saveContinueUri(HttpServletRequest request, HttpServletResponse response);
    String loadContinueUri(HttpServletRequest request);
    String removeContinueUri(HttpServletRequest request, HttpServletResponse response);
}
