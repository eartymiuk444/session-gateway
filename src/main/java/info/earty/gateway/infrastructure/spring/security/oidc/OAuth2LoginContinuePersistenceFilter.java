package info.earty.gateway.infrastructure.spring.security.oidc;

import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OAuth2LoginContinuePersistenceFilter extends OncePerRequestFilter {

    private final OAuth2AuthorizationRequestResolver oAuth2AuthorizationRequestResolver;
    private final ContinueRepository continueRepository;

    public OAuth2LoginContinuePersistenceFilter(OAuth2AuthorizationRequestResolver oAuth2AuthorizationRequestResolver,
                                                ContinueRepository continueRepository) {
        this.oAuth2AuthorizationRequestResolver = oAuth2AuthorizationRequestResolver;
        this.continueRepository = continueRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (this.oAuth2AuthorizationRequestResolver.resolve(httpServletRequest) != null) {
            this.continueRepository.saveContinueUri(httpServletRequest, httpServletResponse);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
