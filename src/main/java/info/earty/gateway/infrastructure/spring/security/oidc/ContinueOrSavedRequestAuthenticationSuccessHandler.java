package info.earty.gateway.infrastructure.spring.security.oidc;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ContinueOrSavedRequestAuthenticationSuccessHandler extends ContinueAuthenticationSuccessHandler {

    private final RequestCache requestCache;

    public ContinueOrSavedRequestAuthenticationSuccessHandler(ContinueRepository continueRepository, RequestCache requestCache) {
        super(continueRepository);
        this.requestCache = requestCache;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        SavedRequest savedRequest = this.requestCache.getRequest(request, response);
        if (savedRequest == null) {
            super.onAuthenticationSuccess(request, response, authentication);
            return;
        }
        String continueUri = this.getContinueRepository().loadContinueUri(request);
        if (continueUri != null) {
            this.requestCache.removeRequest(request, response);
            super.onAuthenticationSuccess(request, response, authentication);
            return;
        }

        String targetUrl = savedRequest.getRedirectUrl();
        getRedirectStrategy().sendRedirect(request, response, targetUrl);
    }
}
