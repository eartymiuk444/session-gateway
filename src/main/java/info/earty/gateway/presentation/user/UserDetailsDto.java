package info.earty.gateway.presentation.user;

import lombok.Data;

import java.util.Set;

@Data
public class UserDetailsDto {
    private String userName;
    private Set<String> authorities;
}
