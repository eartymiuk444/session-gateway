package info.earty.gateway.presentation.attachment;

import info.earty.attachment.presentation.AttachmentCommandApi;
import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@PreAuthorize("hasRole('ADMIN')")
@JaxRsProxyError
public class AttachmentCommandServiceApi implements AttachmentCommandApi, CxfServiceApiProxy {

    private final AttachmentCommandApi attachmentCommandApi;

    @Override
    public void create(Attachment attachment, String workingCardId) {
        attachmentCommandApi.create(attachment, workingCardId);
    }
}
