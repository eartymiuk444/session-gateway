package info.earty.gateway.presentation.content;

import info.earty.sitemenu.presentation.SiteMenuQueryApi;
import info.earty.sitemenu.presentation.data.SiteMenuJsonDto;
import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@JaxRsProxyError
public class SiteMenuQueryServiceApi implements SiteMenuQueryApi, CxfServiceApiProxy {

    private final SiteMenuQueryApi siteMenuQueryApi;

    @Override
    public SiteMenuJsonDto get() {
        return siteMenuQueryApi.get();
    }
}
