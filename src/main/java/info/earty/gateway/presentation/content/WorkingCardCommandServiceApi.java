package info.earty.gateway.presentation.content;

import info.earty.workingcard.presentation.WorkingCardCommandApi;
import info.earty.workingcard.presentation.command.ChangeTextJsonCommand;
import info.earty.workingcard.presentation.command.RemoveAttachmentJsonCommand;
import info.earty.workingcard.presentation.command.RemoveImageJsonCommand;
import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@PreAuthorize("hasRole('ADMIN')")
@JaxRsProxyError
public class WorkingCardCommandServiceApi implements WorkingCardCommandApi, CxfServiceApiProxy {

    private final WorkingCardCommandApi workingCardCommandApi;

    @Override
    public void changeText(ChangeTextJsonCommand jsonCommand) {
        workingCardCommandApi.changeText(jsonCommand);
    }

    @Override
    public void removeImage(RemoveImageJsonCommand jsonCommand) {
        workingCardCommandApi.removeImage(jsonCommand);
    }

    @Override
    public void removeAttachment(RemoveAttachmentJsonCommand jsonCommand) {
        workingCardCommandApi.removeAttachment(jsonCommand);
    }

}
