package info.earty.gateway.presentation.content;

import info.earty.sitemenu.presentation.SiteMenuCommandApi;
import info.earty.sitemenu.presentation.command.*;
import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@PreAuthorize("hasRole('ADMIN')")
@JaxRsProxyError
public class SiteMenuCommandServiceApi implements SiteMenuCommandApi, CxfServiceApiProxy {

    private final SiteMenuCommandApi siteMenuCommandApi;

    @Override
    public void create() {
        siteMenuCommandApi.create();
    }

    @Override
    public void addDirectory(AddDirectoryJsonCommand jsonCommand) {
        siteMenuCommandApi.addDirectory(jsonCommand);
    }

    @Override
    public void moveDirectoryUp(MoveDirectoryUpJsonCommand jsonCommand) {
        siteMenuCommandApi.moveDirectoryUp(jsonCommand);
    }

    @Override
    public void moveDirectoryDown(MoveDirectoryDownJsonCommand jsonCommand) {
        siteMenuCommandApi.moveDirectoryDown(jsonCommand);
    }

    @Override
    public void moveDirectory(MoveDirectoryJsonCommand jsonCommand) {
        siteMenuCommandApi.moveDirectory(jsonCommand);
    }

    @Override
    public void addPage(AddPageJsonCommand jsonCommand) {
        siteMenuCommandApi.addPage(jsonCommand);
    }

    @Override
    public void movePageUp(MovePageUpJsonCommand jsonCommand) {
        siteMenuCommandApi.movePageUp(jsonCommand);
    }

    @Override
    public void movePageDown(MovePageDownJsonCommand jsonCommand) {
        siteMenuCommandApi.movePageDown(jsonCommand);
    }

    @Override
    public void movePage(MovePageJsonCommand jsonCommand) {
        siteMenuCommandApi.movePage(jsonCommand);
    }

    @Override
    public void removeDirectory(RemoveDirectoryJsonCommand jsonCommand) {
        siteMenuCommandApi.removeDirectory(jsonCommand);
    }

    @Override
    public void removePage(RemovePageJsonCommand jsonCommand) {
        siteMenuCommandApi.removePage(jsonCommand);
    }

    @Override
    public void editDirectory(EditDirectoryJsonCommand jsonCommand) {
        siteMenuCommandApi.editDirectory(jsonCommand);
    }

    @Override
    public void changePagePathSegment(ChangePagePathSegmentJsonCommand jsonCommand) {
        siteMenuCommandApi.changePagePathSegment(jsonCommand);
    }
}
