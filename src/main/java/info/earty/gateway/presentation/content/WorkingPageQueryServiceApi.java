package info.earty.gateway.presentation.content;

import info.earty.workingpage.presentation.WorkingPageQueryApi;
import info.earty.workingpage.presentation.data.DraftJsonDto;
import info.earty.workingpage.presentation.data.PublishedPageJsonDto;
import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@JaxRsProxyError
public class WorkingPageQueryServiceApi implements WorkingPageQueryApi, CxfServiceApiProxy {

    private final WorkingPageQueryApi workingPageQueryApi;

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public DraftJsonDto getDraft(String workingPageId) {
        return workingPageQueryApi.getDraft(workingPageId);
    }

    @Override
    public PublishedPageJsonDto getPublishedPage(String workingPageId) {
        return workingPageQueryApi.getPublishedPage(workingPageId);
    }
}
