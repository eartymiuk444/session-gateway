FROM ibm-semeru-runtimes:open-17-jre
COPY target/session-gateway-1.1.0.jar session-gateway-1.1.0.jar
ENTRYPOINT ["java","-jar","/session-gateway-1.1.0.jar"]